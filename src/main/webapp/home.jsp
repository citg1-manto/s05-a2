<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
</head>
<body>
   <%
      String fullname = session.getAttribute("login").toString();
      String emp_or_app = session.getAttribute("applicant_or_employer").toString();
      if(emp_or_app.equals("Applicant")){
    	  emp_or_app="Welcome applicant, You may now start looking for your career opportunity";
      }
      else{
    	  emp_or_app="Welcome employer, You may now start browsing applicant profile";
      }    
   %>
   <h1>Welcome <%= fullname%>!</h1>
   <p> <%= emp_or_app%></p>
   

</body>
</html>