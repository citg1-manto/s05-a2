<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
      <title>Registration Confirmation</title>
</head>
      <body>
      <%
       String discover = session.getAttribute("disc").toString();
        
         if(discover.equals("friends")){
        	 discover="Friends";
         }
         else if (discover.equals("social")){
        	 discover="Social Media";
         }
         else{
        	 discover="Others";
         }
         String birthDate = session.getAttribute("date_of_birth").toString();
         String firstname = session.getAttribute("firstName").toString();
         String lastname = session.getAttribute("lastName").toString();
      %>
      
      <h1>Registration Confirmation</h1>
      <p>First Name: <%= firstname%></p>
      <p>Last Name: <%= lastname%></p>
      <p>Phone: <%= session.getAttribute("phone")%></p>
      <p>Email: <%= session.getAttribute("email")%></p>
      <p>App Discovery: <%= discover%></p> 
      <p>Date of Birth: <%= birthDate %></p>
      <p>User Type: <%= session.getAttribute("applicant_or_employer")%></p>
      <p>Description: <%= session.getAttribute("profile_description")%></p>
       
       <!-- Submit button for booking -->
        <form action="loginIn" method ="post">
        <input type="submit">
         </form>
         
       <!-- Button to go back to index.jsp -->
        <form action="index.jsp" > 
        <input type="submit" value="Back">
        </form>
</body>
</html>