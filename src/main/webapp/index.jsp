<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <title>JSP Job finder</title>
 <style>
  div {
      margin-top: 5px;
      margin-bottom: 5px;
     } 
  
 </style>
  </head>
   <body>
   <div id = "container">
    <h1>Welcome to Servlet Job Finder!</h1>
     <form action = "job_finder" method= "post">
       
       <!-- First Name  -->
         <div>
          <label for= "firstName"> First Name</label>
            <input type= "text" name = "firstName">
             </div>
             
       <!-- Last Name  -->
         <div>
          <label for= "lastName"> Last Name</label>
            <input type= "text" name = "lastName">
             </div>      
        
       <!-- Phone number  -->
         <div>
          <label for= "phone"> Phone Number </label>
           <input type= "text" name = "phone">
            </div>
       
        <!-- Email  -->
          <div>
           <label for= "email"> Email </label>
            <input type= "text" name = "email">
             </div>
       
       <!-- Discovery Choice -->
       <fieldset>
         <legend> How did you discover the app?</legend>
           
         <!-- Friends -->
          <input type="radio" id="friend" name="disc" value= "friends" required>
            <label for="friend">Friends</label>
            <br>
         <!-- Social Media -->
          <input type="radio" id="social_media" name="disc" value= "social" required>
            <label for="social_media" >Social Media</label>
            <br>
         <!-- Other -->
          <input type="radio" id="other" name="disc" value= "others" required>
            <label for="other" >Others</label>
        
      </fieldset>  
      
          <!-- Date of Birth -->
           <div> 
            <label for = "date_of_birth">Date of Birth</label>
            <input type = "date" name = "date_of_birth" required>
           </div>
           
          <!-- Are you an employer or applicant  -->
		<!-- <div>
			 <label for="applicant_or_employer">Are you an employer or applicant</label>
				<input type="text" name="applicant_or_employer" required list="are_you">	
				<datalist id="are_you">
				  <option value="Applicant"selected="selected">
				   <option value="Employer">
				</datalist>
			</div>
			-->
		<div>
          <label for = "applicant_or_employer">Are you an employer or applicant? </label>
           <select id="employee_or_employer" name = "applicant_or_employer" required>   
			 <option value="Employer" selected="selected">Employer</option>
			 <option value="Applicant"selected="selected">Applicant</option>
           </select>
        </div>	
        
		<!-- Special Instructions -->
			<div>
				<label for="profile_description">Profile Description</label>
				<textarea name= "profile_description" maxlength="1000"></textarea>
			</div>
				
		<button>Register</button>
     </form> 
   </div>
 </body>
</html>