package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/job_finder")
public class RegisterServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6116303543789795413L;
	public void init()throws ServletException{
	      System.out.println("RegisterServlet has been initialized");
		}
		public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
			
			String fname = req.getParameter("firstName");
			String lname = req.getParameter("lastName");
	        String phone = req.getParameter("phone");
			String email = req.getParameter("email");
			String discover = req.getParameter("disc");
			String DateOfBirth = req.getParameter("date_of_birth");
			String ApplicantOrEmployer = req.getParameter("applicant_or_employer");
		    String prof_desc = req.getParameter("profile_description");
		    
		    // Store all the data from the form into the session
		    
		    
		    HttpSession session = req.getSession();
		    session.setAttribute("firstName", fname);
		    session.setAttribute("lastName", lname);
		    session.setAttribute("phone", phone);
		    session.setAttribute("email", email);
		    session.setAttribute("disc", discover);
		    session.setAttribute("date_of_birth", DateOfBirth);
		    session.setAttribute("applicant_or_employer", ApplicantOrEmployer);
		    session.setAttribute("profile_description", prof_desc);
		    
		    ServletContext srvContext= getServletContext();
			srvContext.setAttribute("firstName", fname);
			srvContext.setAttribute("lastName", lname);
			srvContext.setAttribute("applicant_or_employer", ApplicantOrEmployer);
		    
		    res.sendRedirect("register.jsp");
		}
		public void destroy() {
			 System.out.println("RegisterServlet has been finalize");
		}

}
