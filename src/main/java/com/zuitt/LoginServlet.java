package com.zuitt;

import java.io.IOException;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/loginIn")
public class LoginServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7509065314802595223L;
	//	  ArrayList<String> login = new ArrayList<>();
	public String login;

	public void init ()throws ServletException{
       System.out.println("Login Servlet has been Initialized");

	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		ServletContext srvContext = getServletContext();
	    String fname = srvContext.getAttribute("firstName").toString();
	    String lname = srvContext.getAttribute("lastName").toString();
	    String emp_Or_app = srvContext.getAttribute("applicant_or_employer").toString();
		String fullname = fname + " " + lname;
		login=fullname;
		
		HttpSession session = req.getSession();
		session.setAttribute("login", login);
		session.setAttribute("applicant_or_employer", emp_Or_app);
		
		res.sendRedirect("home.jsp");
	}
	
	public void destroy() {
		System.out.println("Login Servlet has been finalized");
	}
}
